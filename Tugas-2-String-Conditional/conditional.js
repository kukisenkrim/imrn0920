console.log("---If-else---");
var nama = "Sam";
var peran = "";

if(nama == "" && peran == "" || nama == ""){
    console.log("Nama harus diisi!");
}else if(peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Penyihir "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!");
}else if(peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Guard "+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if(peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Werewolf "+ nama +", Kamu akan memakan mangsa setiap malam!");
}else{
    console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
}

console.log("---Switch Case---");
var tanggal = 1; 
var bulan = 5; 
var tahun = 1998;

switch(bulan){
    case 1: {bulan="Januari";break;}
    case 2: {bulan="Februari";break;}
    case 3: {bulan="Maret";break;}
    case 4: {bulan="April";break;}
    case 5: {bulan="Mei";break;}
    case 6: {bulan="Juni";break;}
    case 7: {bulan="Juli";break;}
    case 8: {bulan="Agustus";break;}
    case 9: {bulan="September";break;}
    case 10: {bulan="Oktober";break;}
    case 11: {bulan="November";break;}
    case 12: {bulan="Desember";break;}
}

if(tanggal <1 || tanggal >31 || bulan <1 || bulan >12 || tahun <1900 || tahun >2200){
    console.log("Silahkan masukkan sesuai range dalam tugas!");
}else{
    console.log(tanggal,bulan,tahun);
}